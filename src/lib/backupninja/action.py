# -*- mode: python; sh-basic-offset: 4; indent-tabs-mode: nil; -*-
# vim: set filetype=python sw=4 sts=4 expandtab autoindent:
#
#    Backupninja python reimplementation, based on original backupninja program
#    by riseup.net.
#    Copyright (C) 2010  Matthijs Kooijman <matthijs@stdin.nl>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

""" Running backup actions """

import os
import logging
import logging as log

from backupninja import config
from backupninja import handlers

def run_all_actions(opts, global_config):
    """
    Run all defined actions.

    opts are the parsed commandline options, global_config is the parsed
    global configuration.
    """
    log.info('Running all actions')
    try:
        action_configs = config.list_actions(opts)
    except OSError, e:
        log.critical('Unable to list actions: %s', e)
        return

    action_configs.sort()

    for action_config in action_configs:
        run_action(action_config, opts, global_config)

def run_action(action_config, opts, global_config):
    """
    Run a single action. action_config is the full path to its
    configuration file. opts are the parsed commandline options,
    global_config is the parsed global configuration.
    """
    config_name = os.path.basename(action_config)
    # Split the action filename
    parts = os.path.basename(config_name).split('.')
    if (len(parts) != 2):
        log.error('Invalid action filename: "%s". Should be in the form name.type, where type is a valid handler.' % action_config)
        return
    (action_name, action_ty) = parts

    action_log = logging.getLogger(config_name)
    action_log.info('Running')

    try:
        # Create a handler for this action
        action = handlers.create_action(action_ty, logger=action_log)
        # Let the handler load its configuration file
        action.load_config(action_config)
        # Run it
        action.run(test=opts.test)
        action.finish(test=opts.test)
        # Ask the action if there where any failures
        success = not action.failed
    except Exception, e:
        action_log.error('Unexpected exception: %s', e)
        import traceback
        log.debug(traceback.format_exc())
        success = False

    if success:
        action_log.info('Succeeded')
    else:
        action_log.info('Failed')
