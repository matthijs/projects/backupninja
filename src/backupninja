#!/usr/bin/python
# -*- mode: python; sh-basic-offset: 4; indent-tabs-mode: nil; -*-
# vim: set filetype=python sw=4 sts=4 expandtab autoindent:
#
#    Backupninja python reimplementation, based on original backupninja program
#    by riseup.net.
#    Copyright (C) 2010  Matthijs Kooijman <matthijs@stdin.nl>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

""" Check for scheduled backupninja actions and run them when needed """

import optparse
import logging
import sys

from backupninja.log import setup_logging
from backupninja import config
from backupninja import action

log = logging.getLogger()

def make_option_parser():
    description = """%prog checks for scheduled actions and runs them when needed."""
    parser = optparse.OptionParser(description=description)
    parser.add_option( "-c", "--confdir"
                     , help="Load configuration from CONFIG_DIR "
                            "instead of '%default'. When "
                            "the --conffile or --actionsdir options "
                            "specify relative paths, they are taken "
                            "relative to this directory."
                     , default="/etc/backupninja"
                     , dest="config_dir"
                     , metavar="CONFIG_DIR")
    parser.add_option( "-f", "--conffile"
                     , help="Load the main configuration from FILE instead "
                            "of '%default'. If FILE is a relative path, it "
                            "is interpreted relative to CONFIG_DIR."
                     , default='backupninja.conf'
                     , dest="global_config"
                     , metavar="FILE")
    parser.add_option( "-a", "--actionsdir"
                     , help="Load action configurations from ACTIONS_DIR "
                            "instead of '%default'. If ACTIONS_DIR is a "
                            "relative path, it is interpreted relative to "
                            "CONFIG_DIR."
                     , default="actions"
                     , dest="actions_dir"
                     , metavar="ACTIONS_DIR")
    parser.add_option( "-d", "--debug"
                     , help="Run in debug mode. This only means logging is "
                            "more verbose."
                     , action="store_true"
                     , dest="debug")
    parser.add_option( "-t", "--test"
                     , help="Run in testing mode. This means no actions "
                            "are taken, only show what would be done "
                            "(as far as possible without taking "
                            "actions, of course)."
                     , action="store_true"
                     , dest="test")
    return parser
 
def main(argv):
    # Parse options (Note that this exits in case of --help or an
    # invalid commandline)
    parser = make_option_parser()
    (options, args) = parser.parse_args(argv)

    # Setup logging
    setup_logging(options)

    # Load config file
    global_conf = config.get_global_config(options)
    if global_conf is None:
        # Error was already logged
        return 1

    # Process command
    action.run_all_actions(options, global_conf)

    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))
