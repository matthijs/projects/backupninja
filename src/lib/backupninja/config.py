# -*- mode: python; sh-basic-offset: 4; indent-tabs-mode: nil; -*-
# vim: set filetype=python sw=4 sts=4 expandtab autoindent:
#
#    Backupninja python reimplementation, based on original backupninja program
#    by riseup.net.
#    Copyright (C) 2010  Matthijs Kooijman <matthijs@stdin.nl>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

""" Load configuration for backupninja and configured actions """

import os, ConfigParser

# Defaults for global configuration values
default_global_config = {}

import logging as log

class ConfigError(Exception):
    """
    An exception thrown when something is wrong with the config.
    This is not thrown by the config module, but it is meant to be
    thrown by handlers when they find something wrong with the
    configuration contents.
    """
    pass

def get_global_config(opts):
    """
    Returns the global configuration, in a SafeConfigParser object.
    If the configuration file can not be found, logs an error and
    returns None.

    opts are the parsed commandline options.
    """
    global_config = os.path.join(opts.config_dir, opts.global_config)
    return load_config(global_config, default_global_config)

def list_actions(opts):
    """
    Lists all actions defined in the configuration directory. Returns a
    list of full paths to action configuration files.

    opts are the parsed commandline options.
    """
    actions_dir = os.path.join(opts.config_dir, opts.actions_dir)
    return [os.path.join(actions_dir, f) 
            for f in os.listdir(actions_dir) 
            if not f.startswith('.')]
    
def load_config(filename, defaults):
    """
    Load a configuration file, using the given default values.

    The defaults argument contains a dictionary of sections. Each key is
    a section name, each value is a dictionary of values (where the key
    is the value name and the value is the actual value).
    """
    # Open a file and read it
    config = ConfigParser.SafeConfigParser()
    _set_default_config(config, defaults)
    log.debug('Reading config file "%s"', filename)
    try:
        file = open(filename, 'r')
    except IOError, e:
        # Log the error and return None
        msg = 'Unable to open configuration file "%s": %s' % (filename, e)
        log.error(msg)
        return None

    config.readfp(file)
    return config

def _set_default_config(parser, values):
    """
    Saves the values given to the ConfigParser given. This can be used
    to store a set of default values to a ConfigParser before loading a
    file (The defaults argument to the ConfigParser constructor only
    sets the values of the "DEFAULT" section).

    The values argument contains a dictionary of sections. Each key is a
    section name, each value is a dictionary of values (where the key is
    the value name and the value is the actual value).
    """
    for section, options in values.items():
        if not parser.has_section(section):
            parser.add_section(section)
        for option, value in options.items():
            # Interpret None as "no default", since ConfigParser doesn't
            # like non-string values.
            if not value is None:
                parser.set(section, option, value)
