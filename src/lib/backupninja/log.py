# -*- mode: python; sh-basic-offset: 4; indent-tabs-mode: nil; -*-
# vim: set filetype=python sw=4 sts=4 expandtab autoindent:
#
#    Backupninja python reimplementation, based on original backupninja program
#    by riseup.net.
#    Copyright (C) 2010  Matthijs Kooijman <matthijs@stdin.nl>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

""" Sets up the python logging library for use with backupninja """

import logging
log = logging.getLogger()

def setup_logging(options):
    """
    Setup the logging library, so other modules can just use
    logging.getLogger or the root logger directly.
    
    options are the parsed commandline options.
    """
    # We use the default options for now
    level = logging.INFO
    if(options.debug):
        level = logging.DEBUG
    logging.basicConfig(level=level)
    log.debug("Initialized logging configuration")

def log_exception(log=None, msg="%s"):
    """
    This is a decorator that catches an exception, logs the exception
    and a backtrace and then swallows it.  The exception is logged using
    the "error" level for the message, and "debug" for the backtrace.

    log is the Logger instance to log to. If this is not present, the
    decorated function should be a method of an object that contains a
    "log" attribute that contains a Logger instance.

    msg is the message to log (which must contain a %s into which the
    exception message is interpolated).

    """
    def decorator(f):
        def inner(*args, **kwargs):
            try:
                f(*args, **kwargs)
            except Exception, e:
                # Find out which logger to use. We create a new variabel
                # logger here, since it seems to be impossible to assign
                # the log variable from the log_exception scope.
                if log is None:
                    # No log is passed. Get the self argument (args[0]) and
                    # get is "log" attribute.
                    logger = args[0].log
                else:
                    # Use the passed log
                    logger = log

                logger.error(msg, e)
                import traceback
                logger.debug(traceback.format_exc())
        return inner
    return decorator
