# -*- mode: python; sh-basic-offset: 4; indent-tabs-mode: nil; -*-
# vim: set filetype=python sw=4 sts=4 expandtab autoindent:
#
#    Backupninja python reimplementation, based on original backupninja program
#    by riseup.net.
#    Copyright (C) 2010  Matthijs Kooijman <matthijs@stdin.nl>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

""" Testing handler that logs a configurable message """

from backupninja.handlers import Action

class TestAction(Action):
    def run(self, **kwargs):
        self.log.info(self.conf.get('main', 'message'))
        
handler = TestAction
